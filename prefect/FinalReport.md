# Distributed ETL Pipeline for IEX PCAP Data Processing
## Introduction
In the realm of high-frequency trading and financial analytics, the need for rapid data processing is paramount. IEX PCAP data, which stands for Packet Capture data from the Investors Exchange, is a goldmine for quantitative analysts and traders alike. This project dives into creating an advanced, distributed ETL (Extract, Transform, Load) pipeline to harness the power of this data - by downloading, parsing, and running computations on it.

## Motivation
As the volume of financial data continues to grow exponentially, a robust infrastructure is essential to handle, process, and derive insights from it. Our project uses the Prefect framework to ensure efficient and fault-tolerant data workflows, coupled with Dask for parallel computing power.

## Environment Setup
### System Prerequisites
Before setting up the pipeline, ensure your environment is optimized for the task:

#### Volume Configuration
Ensure you have allocated sufficient storage for the virtual machines.
Redirect Vagrant's machine folder to a path with adequate storage:
```
VBoxManage setproperty machinefolder </new/path>
```
#### Inter-VM SSH Setup
For seamless operations, the VMs need to communicate via SSH. Set it up with:

```
# On worker node:
sudo sed -i 's/^#\?PasswordAuthentication .*/PasswordAuthentication yes/' /etc/ssh/sshd_config
ssh-keygen

# On master node:
ssh-copy-id VM_IP_1
ssh-copy-id VM_IP_2
...
```

### Cluster Initialization
Starting the environment is straightforward:

#### Repository Setup
Clone the provided repository and initiate Vagrant:

```
git clone <repository_url>
vagrant up
```
#### Deployment Script
Kickstart the ETL deployment by running:
```
bash make_deployments.sh
```

This script sets up the Prefect and Dask cluster that support in-cluster communication and task scheduling, and also make an example prefect deployment. For subsequent ETL tasks, invoke:

```
vagrant ssh -c master "sudo python workflow/deploy.py"
```

## Project Workflow
### Prefect: The ETL Powerhouse
Prefect is the centerpiece of our ETL processes. It's a modern workflow orchestration platform that natively integrates with Dask. This combination ensures our tasks are not only well-orchestrated but also parallelized and distributed across the cluster.

### Virtual Environment: Vagrant & VirtualBox
Our project simulates a real-world distributed environment using Vagrant for virtual machine management and VirtualBox for virtualization. The provided Vagrantfile is tailored for our needs, with configurations for the master and worker nodes. These nodes have been assigned dedicated roles and resources to mirror a production environment.

### Data Processing
The ETL pipeline follows these steps:

#### Extraction: Downloading IEX PCAP data.
#### Transformation: Parsing the raw data.
#### Load: Performing computations and storing results.
The distributed nature of our setup shines here, especially when dealing with vast amounts of PCAP data. With tasks distributed across nodes, the processing is faster and more efficient.


## Prefect
Prefect is a modern dataflow automation framework that aims to simplify and streamline the creation, deployment, and management of complex data workflows. It is designed to handle failures and unexpected exceptions gracefully, ensuring that your data pipeline remains resilient. Prefect offers both an open-source engine for creating workflows and a commercial platform for orchestration and deployment.

### Advantages of Prefect
1. Error handling: Prefect was designed to handle failures and errors out of the box. This greatly reduces the time spent on debugging and fixing pipelines.
2. Flexibility: Prefect is highly flexible and allows for dynamic pipeline creation. You can create complex workflows that can change dynamically based on the data or the state of other tasks.
3. Ease of use: Prefect has an intuitive Pythonic interface and provides good abstractions for task dependencies.
4. Scalability: Prefect can scale to support large workflows, both in terms of the number of tasks and the amount of data.
5. Cloud-native: Prefect's commercial offering, Prefect Cloud, provides a robust platform for deploying and managing your workflows in the cloud.

### Disadvantages of Prefect
1. Learning curve: While Prefect's Pythonic interface is intuitive, it can still have a learning curve, particularly for complex workflows.
2. Community support: Prefect is newer than other workflow management systems, so it may not have as large a community or as many resources available.
3. Lack of certain features: There are certain features available in other workflow management systems that are not yet available in Prefect. For example, when doing distributed workflow, prefect does not support naively for different processes among different vms. One should first running a Dask cluster among vms, and then build a distributed prefect based ono that

### Comparisons
Prefect vs Airflow: Prefect was actually built by some of the same developers as Airflow, and it was designed to address some of the shortcomings they saw in Airflow. For example, Prefect has a more intuitive interface and better error handling out of the box. However, Airflow has been around longer and has a larger community and more plugins available.

Prefect vs Toil: Toil is a workflow engine for computational (particularly bioinformatics) workflows. It focuses on portable, reproducible runs and supports a variety of computing backends including grid computing and cloud computing. Prefect, on the other hand, is more general-purpose and is designed for a wide range of data workflows. It provides a higher-level, more intuitive abstraction for defining workflows, whereas Toil workflows can be more complex to define but offer lower-level control.

## Conlusion
Through this project, we've showcased how to effectively set up and run a distributed ETL pipeline for IEX PCAP data. Using Prefect, Vagrant, and VirtualBox, we've mimicked a scalable production environment on a local machine. This setup can be further expanded with more nodes, allowing for even greater data processing capability. The configurations and scripts provided can serve as a blueprint for similar data-intensive projects.