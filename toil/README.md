# Toil with CWL 
This directory contains a CWL Toil workflow script that runs on a single machine.

## Prerequisite
Make sure you have enought volumes for your vms

## To Start 
cd toil
sh ./run_vm.sh

## Output
The output will be stored under current directory

## To Start
```
cd toil
sh ./run_vm.sh
```

