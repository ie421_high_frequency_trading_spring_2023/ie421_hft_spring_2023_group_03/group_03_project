#!/bin/bash

today=$(date +%Y-%m-%d)
zcat $2 | python -u $1 -i /dev/stdin -o "$today.csv"