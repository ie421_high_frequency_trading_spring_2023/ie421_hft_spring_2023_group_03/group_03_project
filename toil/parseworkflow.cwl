#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow
      
inputs:
  parse_data_script:
    type: File

  download_file:
    type: File

  parse_iex_pcap_script:
    type: File

  stat_data_script:
    type: File

  candle_chart_script:
    type: File

steps:
  parse:
    run: steps/parse.cwl
    in: 
      parse_data_script_file: parse_data_script
      download_zip: download_file
      parse_iex_pcap_script_file: parse_iex_pcap_script
    out: [book_snapshot1,book_snapshot2,text_tick_data]

  stat:
    run: steps/stat.cwl
    in:
      stat_data_script_file: stat_data_script
      candle_chart_script_file: candle_chart_script
      book_snapshot2_file: parse/book_snapshot2
    out: [csv_file]

outputs: 
  book_snapshot1:
    type: File
    outputSource: parse/book_snapshot1
  book_snapshot2:
    type: File
    outputSource: parse/book_snapshot2
  text_tick_data:
    type: File
    outputSource: parse/text_tick_data
  csv_file:
    type: File
    outputSource: stat/csv_file
