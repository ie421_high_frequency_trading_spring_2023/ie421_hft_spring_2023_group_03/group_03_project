#!/bin/bash

# Download Mesos
sudo wget https://downloads.apache.org/mesos/1.11.0/mesos-1.11.0.tar.gz
sudo tar -zxf mesos-1.11.0.tar.gz
# Mesos system requirement set-up
sudo apt-get install -y tar wget git
sudo apt-get install -y openjdk-8-jdk
sudo apt-get install -y autoconf libtool
sudo apt-get -y install build-essential python-dev python-six python-virtualenv libcurl4-nss-dev libsasl2-dev libsasl2-modules maven libapr1-dev libsvn-dev zlib1g-dev iputils-ping

# Build Mesos
cd build
IP=$(ip addr show eth0 | grep 'inet ' | awk '{ print $2 }' | cut -d/ -f1)
sudo ./bin/mesos-agent.sh --master=$IP:5050 --work_dir=/var/lib/mesos
