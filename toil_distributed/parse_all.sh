
PYTHON_INTERP=python3.9


for pcap in $(ls data/*gz);
do
	pcap_date=$(echo $pcap | sed -r 's/.*data_feeds_(.*)_(.*)_IEXTP.*/\1/')
	echo "PCAP_FILE=$pcap PCAP_DATE=$pcap_date"
	#gunzip -d -c $pcap | tcpdump -r - -w - -s 0 | $PYTHON_INTERP src/parse_iex_pcap.py /dev/stdin --symbols SPY
	gunzip -d -c $pcap | tcpdump -r - -w - -s 0 | $PYTHON_INTERP src/parse_iex_pcap.py /dev/stdin --symbols SPY --trade-date $pcap_date --output-deep-books-too

done;

