#!/bin/bash

vagrant up 
vagrant ssh master -c 'cd /workflow ; sh ./build_mesos_master.sh'
vagrant ssh worker1 -c 'cd /workflow ; sh ./build_mesos_agent.sh'