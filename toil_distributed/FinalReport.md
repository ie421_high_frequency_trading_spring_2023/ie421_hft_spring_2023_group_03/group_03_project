# IE421 Group3 Toil Workflow Final Report
## Author: Haozhen Zheng

Toil is an open-source workflow<sup>1</sup> engine designed to facilitate the efficient execution of scientific computations and data analyses pipelines across various computing platforms at large scale. Notably, Toil stands out as the first software to implement the Common Workflow Language (CWL) and provide draft support for the Workflow Description Language (WDL). Additionally, Toil offers a versatile Python API that enables users to define workflows either statically or dynamically. Both CWL and the Python API support the integration of Docker containers, simplifying the sharing of programs without necessitating external installation and configuration.

Toil runs within multiple settings:  
* Cloud environment
    - AWS, Microsoft Azure, Google Cloud, Open Stack
* HPC
    - GridEngine, Slurm
* Distributed systems 
    - Apache Mesos

This research projects specifically focus on runing Toil distributed across multiple Virtual Machines by communicating with Apache Mesos (A distributed systems kernel). In the following discussion, we will concentrate specifically on the distributed setting. This will include an exploration of the architectures, technologies, and methodologies relevant to distributed systems, as well as the unique challenges and solutions associated with operating in a distributed environment.

![Alt text](fig/image.png)
Pic from Apache Mesos\
Toil acts like the Hadoop scheduler that schdule jobs for Mesos.

**Highlight: Apache Mesos is the only resource that helps Toil scale its workflow across distributed systems according to its paper.**

<sup>1</sup>workflow is composed of a set of tasks and jobs that are orchestrated
by specification of a set of dependencies that map the inputs and outputs between jobs. 

---

## Architecture

Toil is built in a modular way and consists of three main configurable pieces:

1. **Job Store API**: A filepath or URL for hosting and centralizing all files for a workflow (e.g., a shared directory across multiple machines, or an AWS S3 bucket URL).
2. **Batch System API**: Specifies the execution environment (local, lsf, parasol, mesos, slurm, torque, htcondor, kubernetes, or grid_engine).
3. **Provisioner**: Only for cloud-based operations, specifies which cloud provider to use.

---

## Efficiency Implementation
Toil employs a master/agents architecture for job scheduling, where the master node is responsible for decision-making and task distribution to agent nodes. To alleviate the master's workload, agents provide feedback about their capacity to execute downstream tasks by promptly comparing their available resources against the task requirements and workflow dependencies. Toil enhances resilience to job failures by persisting jobs and caches within the job store—a shared storage space. This allows users to resume interrupted workflows with minimal delay. Upon successful completion of the workflow, the job store is automatically cleared and cleaned up. Additionally, Toil optimizes input/output operations by caching large data streams in the filestore, a subset of the job store, thus avoiding redundant data transfers across agent nodes. These optimizations not only improve Toil's efficiency in terms of time and cost but also underscore its transformative potential in large-scale computational environments.

## Advantages
Toil is an open-source, portable solution designed for large-scale scientific computation workflows. It integrates Docker containers, enabling the sharing of programs without the need for additional installation and configuration, significantly boosting Toil's capacity for transformation. Toil offers a Python API and supports both the Common Workflow Language (CWL) and the Workflow Description Language (WDL), catering to a wide range of computational tasks.

Toil is versatile, supporting various computing environments including single machines, cloud clusters, high-performance computing (HPC) environments, and distributed systems. It enhances reliability through features like job retries, checkpointing, and recovery mechanisms. Additionally, Toil optimizes data handling by caching intermediate files and selectively exporting only those files explicitly specified by the workflow.

Users can monitor ongoing job statuses via Toil's web interface, which also allows for the convenient download of log information to facilitate quick error identification. Moreover, Toil enables dynamic job scaling and autoscaling based on resource requirements, ensuring efficient use of computational resources.



---

## Disadvantages
This section primarily focuses on the distributed setting. Mesos, a kernel for distributed systems, stands as the sole mechanism for scheduling Toil jobs across various virtual machines. However, it lacks direct support for executing CWL scripts. While Toil employs caching to minimize data transfer overhead between nodes, it necessitates the explicit declaration of data elements within the Python script to ensure proper caching within Toil's filestore. To enable Mesos agents to operate on different systems, Mesos must be built from source for each system, requiring uniform installation and configuration of Toil and all necessary Python script modules across these systems. The build process can be significantly slowed by limited RAM allocation and hindered by storage limitations. Additionally, it is worth noting that Toil's GitHub repository is currently not actively maintained, with 361 unresolved issues remaining.


---

## Challenges and Solutions

### Storage and RAM Challenges

During the project (distributed execution of Toil on multiple VMs), I faced significant challenges related to storage and RAM, primarily due to the inefficiency of building Mesos on each Virtual Machine (VM) and system performance issues.

*   **Building Mesos Locally and Mounting to VMs**

    *   **Challenge**: Building Apache Mesos on each VM proved to be excessively slow, leading to system stalls. This inefficiency necessitated an alternative approach to streamline the setup process and enhance performance.

    *   **Solution**: To address this, I built Mesos on my local machine and then mounted these files onto the VMs. This method significantly reduced the build time and system load, making the workflow setup more efficient. However, this process introduced a limitation: it could not be directly replicated on other machines due to numerous absolute path configurations I modified in the source files. These alterations were specific to my local setup and thus not universally applicable without further adjustments.

*   **Handling Invalid Credentials on Mesos Agent**

    *   **Challenge**: Another issue encountered was invalid credentials on the Mesos agent. This problem stemmed from inconsistencies between the user accounts on the VMs and the local machine, leading to authentication failures.

    *   **Solution**: To resolve this challenge, I included the `--switch_user=false` flag in the Mesos command. This adjustment bypassed the need for matching user accounts between the Mesos master and agents, allowing the system to operate without credential mismatches. This workaround was crucial for maintaining seamless communication and operation across the distributed setup.

*   **Library Path Configuration**:
    
    *   **Challenge**: Needed to add custom library paths to `LD_LIBRARY_PATH`.
    *   **Solution**: Exported `LD_LIBRARY_PATH` with custom paths.
*   **Firewall Configuration**:
    
    *   **Challenge**: Required specific ports to be allowed through the firewall.
    *   **Solution**: Used `ufw` to allow necessary ports.
*   **Working Directory Restrictions**:
    
    *   **Challenge**: Encountered errors when the working directory was not under the home directory.
    *   **Solution**: Ensured the working directory was located under the home directory.
*   **Dependency Installation on a New Machine**:
    
    *   **Challenge**: Setting up a new environment was difficult due to dependencies.
    *   **Solution**: Followed Mesos documentation for dependency installation and copied the Mesos source folder to a shared directory.
*   **Software Bugs**:
    
    *   **Challenge**: Faced bugs with `http-parser` during Toil installation and an `AttributeError` with `pyopenssl`.
    *   **Solution**: Installed build essentials and updated `pyopenssl`.
*   **Network Folder Mounting**:
    
    *   **Challenge**: Difficulty in mounting folders on agent VMs.
    *   **Solution**: Installed `nfs-common` and configured network folder sharing.

### Environment Setup

*   **Install Dependencies**: Follow the Mesos documentation for Ubuntu 16.04.
*   **Mesos and Toil Installation**: Address specific software bugs through updates.
*   **Library Path Exportation**: Add custom library paths to `LD_LIBRARY_PATH`.
*   **Firewall Configuration**: Allow necessary ports through the firewall.
*   **Work Directory Configuration**: Ensure work directories are under the home directory.

### Steps for Running the Workflow

1.  **Preparation**:
    *   Vagrant up Virtual Machines.
    *   Install all necessary dependencies(Python>3.7, Toil[all]).
    *   Correctly set library paths and work directories.
    *   Configure the firewall to allow required ports.
2.  **Master and Agent Configuration**:
    
    *   Build Mesos on each system and set up Mesos master and agents with appropriate configurations.
3.  **Network Sharing**:
    
    *   Ensure network sharing and mount shared folders between master and agents.
4.  **Toil Installation and Configuration**:
    
    *   Install and configure Toil, resolving any encountered issues.
5.  **Workflow Execution**:
    
    *   Start Mesos services and ensure network folders are accessible.
    *   Execute the Toil python script on local machine

### Research

Throughout the project, I developed a CWL (Common Workflow Language) script utilizing Toil to execute workflows on a single machine, as well as a Python script designed to schedule jobs and enable distributed execution across multiple virtual machines (VMs). To achieve this, I diligently reviewed the Toil documentation and its source code, gaining a deep understanding of its capabilities and implementation. Additionally, I conducted thorough research on Apache Mesos to facilitate the distribution of workflows, ensuring efficient resource management and scalability.

To demonstrate the feasibility of running Toil in a distributed setting with Mesos's support, I created a demo showcasing the successful integration of these technologies. This required modifying the Mesos source code to adjust path configurations, allowing me to build Mesos only once and subsequently mount these source files into the VMs. This approach streamlined the setup process and proved the effectiveness of Toil and Mesos in a distributed environment.

 The demo for this distributed work is [here](https://drive.google.com/file/d/1_pKVIMPtmarjt9jT4rJaudxxaCVPsC_w/view?usp=sharing)

---

## Conclusion
This project specifically focused on proving Toil's capability for distributed execution across multiple virtual machines (VMs), leveraging Apache Mesos as the backbone for scheduling and managing tasks. Our work effectively demonstrated Toil's robustness in a distributed environment and we also presents many issues throughout the implementation. 

---
## Reference
Rapid and efficient analysis of 20,000 RNA-seq samples with Toil \
John Vivian, Arjun Rao, Frank Austin Nothaft, Christopher Ketchum, Joel Armstrong, Adam Novak, Jacob Pfeil, Jake Narkizian, Alden D. Deran, Audrey Musselman-Brown, Hannes Schmidt, Peter Amstutz, Brian Craft, Mary Goldman, Kate Rosenbloom, Melissa Cline, Brian O’Connor, Megan Hanna, Chet Birger, W. James Kent, David A. Patterson, Anthony D. Joseph, Jingchun Zhu, Sasha Zaranek, Gad Getz, David Haussler, Benedict Paten
bioRxiv 062497; doi: https://doi.org/10.1101/062497



