# Toil Distributed Framework README

This README provides an overview of the scripts included in the Toil Distributed Framework project and instructions on how to use them.

## Scripts

- `run_all.sh`: This master script initiates the entire Toil distributed workflow. It sets up the virtual machines, builds Mesos, and starts the Toil workflow.

- `run_vm.sh`: This script launches virtual machines using Vagrant and installs all necessary packages for the Toil framework to function.

- `build_mesos_agent.sh`: This script builds Mesos on the agent nodes within the virtual machines.

- `build_mesos_master.sh`: This script builds Mesos on the master node within the virtual machines.

- `note`: This directory contains notes detailing the challenges encountered during the project and the solutions that were implemented to showcase Toil's distributed capabilities.

- `FinalReport.md`: The final report for the Toil Distributed Framework project, detailing the architecture, implementation, advantages, and challenges.

## Usage

Please place current directory under `/workflow` directory by
```bash
mkdir /workflow
mv * /workflow/
```

To run the Toil Distributed Framework with a single command, execute the following script in your terminal:

```bash
./run_all.sh
