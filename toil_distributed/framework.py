import os
import tempfile

from toil.common import Toil
from toil.job import Job

import subprocess
import requests
import sys
import os.path
from os import path

import tarfile
from src.download_iex_pcaps import IexMarketDataFile, IexAvailableFiles,IexFileDownloader,download_dates
from toil.fileStores import abstractFileStore
import shutil
from src.compute_candle_chart import compute_candle_chart

import csv

# hc
DATE='20230915'
PATH = '/home/zoe/secret_dir/shared'

class HelloWorld(Job):
    def __init__(self, id=None):
        Job.__init__(self, memory="2G", cores=1, disk="5G")
        self.inputFileID = id
        self.outputFileID = None
    def download_files(self, stdout_output, fileStore):
        outputFileIDs = []
        for file in stdout_output:
            file_url, local_path, _ = file
            r = requests.get(file_url, stream=True, allow_redirects=True)
            total_size = int(r.headers.get('content-length'))
            initial_pos = 0
            tempFile = fileStore.getLocalTempFile()
            self.log(tempFile)
            data_folder_path = os.path.join(PATH, "data")
            if not os.path.exists(data_folder_path):
                os.mkdir(data_folder_path)
            tempFile = os.path.join(data_folder_path,f"{DATE}_trades.csv.gz")
            with open(tempFile,'wb') as f: 
                for ch in r.iter_content(chunk_size=1024):                             
                    if ch:
                        f.write(ch) 
            
            self.outputFileID = fileStore.writeGlobalFile(tempFile)
            self.log(self.outputFileID)
            outputFileIDs.append(self.outputFileID)
# hc
        return outputFileIDs[0]
    def get_Download_File_ID(self):
        return self.outputFileID

    def run(self, fileStore):
# hc
        start_date_str = end_date_str ='2023-09-15'
        download_dir = '.'
        all_deep_file_objects=download_dates(download_dir, start_date_str, end_date_str, 'DEEP')
        outputFileID = self.download_files(all_deep_file_objects, fileStore)
        return outputFileID


class HoliLand(Job):
    def __init__(self, id=None):
        Job.__init__(self, memory="2G", cores=1, disk="5G")
        self.inputFileID = id


    def run(self,fileStore):
        self.log("enter run HoliLand")
        print(subprocess.run(["bash", "/home/zoe/secret_dir/shared/parse_all.sh", self.inputFileID]))
        return self.inputFileID
        
    
class GalbiTang(Job):
    def __init__(self, id=None):
        Job.__init__(self, memory="5G", cores=1, disk="5G")
        self.inputFileID = id

    def run(self, fileStore):
# hc
        date="20230915"
        # importFileID = fileStore.import_file(self.importFileLink)
        # trade_targz = fileStore.readGlobalFile(importFileID)
        trade_targz = fileStore.readGlobalFile(self.inputFileID)
        output_file = compute_candle_chart(trade_targz,f'{date}_candle_chart.csv')
        with open(output_file) as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=',')
            for row in csv_reader:
                print(row)

        outputFileID = fileStore.writeGlobalFile(output_file)
        print(f"GalbiTang fileID: {outputFileID}")
        return outputFileID
    


if __name__ == "__main__":

    parser = Job.Runner.getDefaultArgumentParser()
    options = parser.parse_args()
    options.logLevel = "INFO"
    options.clean = "always"
    
    options.batchSystem = "mesos"
    options.mesosMaster = "192.168.56.7:5050" 
    options.mesosMasterAddress = "192.168.56.7:5050" 

    with Toil(options) as toil:
        ioFileDirectory = os.path.join(os.path.dirname(os.path.abspath(__file__)),"data")
        secret = os.path.join(ioFileDirectory,f'book_snapshots/{DATE}_trades.csv.gz')
        job1 = HelloWorld()
        job2 = HoliLand(id=job1.rv())
        job3 = GalbiTang(id=job2.rv())
        job1.addChild(job2)
        job2.addChild(job3)
        outputFileID = toil.start(job1)
# export stat output
        toil.exportFile(outputFileID, os.path.abspath(os.path.join(ioFileDirectory, f"{DATE}_candle_chart.csv")))
        #toil.exportFile(job1.rv(), os.path.abspath(os.path.join(ioFileDirectory, f"out.tar.gz")))
